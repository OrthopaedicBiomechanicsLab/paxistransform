import os, fnmatch
import string
import vtk, qt, ctk, slicer
import PythonQt
import numpy as np
import subprocess
import sys
import unittest
from slicer.ScriptedLoadableModule import *

from scipy import ndimage
import nibabel as nib

import time

class PAxisTransform(ScriptedLoadableModule):
    def __init__(self, parent):
        ScriptedLoadableModule.__init__(self, parent)
        self.parent.title = "PAxis Transform"
        self.parent.categories = ["Principal Axis Transformation"]
        self.parent.dependencies = []
        self.parent.contributors = ["Tina Wu (Sunnybrook Research Institute)"]
        self.parent.helpText = """
        This module resamples incoming scans and predicts the segmentations for those scans.
        """
        self.parent.acknowledgementText = """
        The principal axes transformation--a method for image registration
        """
class PAxisTransformWidget(ScriptedLoadableModuleWidget):
    def setup(self, tabLayout = None):
        if tabLayout == None:
            ScriptedLoadableModuleWidget.setup(self)
            self.reloadCollapsibleButton.visible = False
            tab = qt.QTabWidget()
            tabLayout = qt.QFormLayout(tab)
            self.layout.addWidget(tab)
            try:
                parent = slicer.util.findChildren(text='Data Probe')[0]
                parent.setChecked(False)
            except IndexError:
                print("No Data Probe frame - cannot collapse")
                return
        self.corticalLabelOn = 1
        self.boneThreshold = 150
        self.roiVolume = None
        self.corticalNode = None
        self.setupDeveloperSection()

        lm = slicer.app.layoutManager()
        redWidget = lm.sliceWidget('Red')
        self.redController = redWidget.sliceController()
        self.redLogic = redWidget.sliceLogic()

        yellowWidget = lm.sliceWidget('Yellow')
        self.yellowController = yellowWidget.sliceController()
        self.yellowLogic = yellowWidget.sliceLogic()

        greenWidget = lm.sliceWidget('Green')
        self.greenController = greenWidget.sliceController()
        self.greenLogic = greenWidget.sliceLogic()
    
        self.filesGroupBox = ctk.ctkCollapsibleGroupBox()
        self.filesGroupBox.title = "Files to Transform"
        tabLayout.addWidget(self.filesGroupBox)
        self.filesGroupBox.collapsed = False
        self.filesGroupBoxLayout = qt.QFormLayout(self.filesGroupBox)

        self.fixedVolmDirSelector = ctk.ctkPathLineEdit()
        #self.fixedVolmDirSelector.filters = ctk.ctkPathLineEdit.Dirs
        self.fixedVolmDirSelector.settingKey = 'DICOMPatcherInputDir'
        self.filesGroupBoxLayout.addRow("Fixed Volume: ", self.fixedVolmDirSelector)

        self.movingVolmDirSelector = ctk.ctkPathLineEdit()
        #self.movingVolmDirSelector.filters = ctk.ctkPathLineEdit.Dirs
        self.movingVolmDirSelector.settingKey = 'DICOMPatcherInputDir'
        self.filesGroupBoxLayout.addRow("Moving Volume: ", self.movingVolmDirSelector)

        self.thresholdInput = qt.QLineEdit("900")
        self.filesGroupBoxLayout.addRow("Threshold: ", self.thresholdInput)

        self.vectorScalingInput = qt.QLineEdit("10")
        self.filesGroupBoxLayout.addRow("Vector Scaling Factor: ", self.vectorScalingInput)

        self.findtf = qt.QPushButton("Find Transform")
        self.filesGroupBoxLayout.addRow(self.findtf)
        self.findtf.connect('clicked(bool)', self.findtfSelected)
        self.findtf.enabled = True

        self.saveData = qt.QCheckBox("Save Mask")
        self.saveData.setCheckState(False)
        self.filesGroupBoxLayout.addRow(self.saveData)

    def findtfSelected(self):
        logic = PAxisTransformLogic()
        movingVolmPath = self.movingVolmDirSelector.currentPath
        fixedVolmPath = self.fixedVolmDirSelector.currentPath
        imageThreshold = float(self.thresholdInput.text)
        vectorScalingFactor = float(self.vectorScalingInput.text)
        saveDataFlag = self.saveData.isChecked()
        logic.run(fixedVolmPath, movingVolmPath, imageThreshold, vectorScalingFactor, saveDataFlag)

class PAxisTransformLogic(ScriptedLoadableModuleLogic):
    def __init__(self):
        self.scalingFactor = 0

        self.movingVolume = None
        self.fixedVolume = None
        self.threshold = 0

        self.movingVolmPath = None
        self.fixedVolmPath = None

        self.movingVolume_slicer = None
        self.fixedVolume_slicer = None

        self.transformFromMtoF = None
        
        self.fixed_ijk_to_ras = np.eye(4)
        self.moving_ijk_to_ras = np.eye(4)

        self.numpy_to_ijk = np.array([0,0,1,0,0,1,0,0,1,0,0,0,0,0,0,1]).reshape([4,4])

    def loadObjects(self, fixedVolmPath, movingVolmPath, imageThreshold, scalingFactor):
        try: 
            print('remove previous nodes...')
            scalarVolumes = slicer.util.getNodesByClass('vtkMRMLScalarVolumeNode')
            for scalarVolume in scalarVolumes:
                slicer.mrmlScene.RemoveNode(scalarVolume)

            models = slicer.util.getNodesByClass('vtkMRMLModelNode')
            for model in models:
                slicer.mrmlScene.RemoveNode(model)

            transforms = slicer.util.getNodesByClass('vtkMRMLTransformNode')
            for tf in transforms:
                slicer.mrmlScene.RemoveNode(tf)

            rulers = slicer.util.getNodesByClass('vtkMRMLAnnotationRulerNode')
            for ruler in rulers:
                slicer.mrmlScene.RemoveNode(ruler)
        except:
            print('continue as normal')
        self.scalingFactor = scalingFactor
        self.threshold = imageThreshold

        self.movingVolume_slicer = self.loadVolume(movingVolmPath)
        self.movingVolume_slicer.SetOrigin([0,0,0])
        self.movingVolume_slicer.SetName('movingVolume_slicer')
        
        self.fixedVolume_slicer = self.loadVolume(fixedVolmPath)
        self.fixedVolume_slicer.SetOrigin([0,0,0])
        self.fixedVolume_slicer.SetName('fixedVolume_slicer')

        # these are np representations of the volumes
        self.movingVolume = slicer.util.arrayFromVolume(slicer.util.getNode('movingVolume_slicer'))
        self.fixedVolume = slicer.util.arrayFromVolume(slicer.util.getNode('fixedVolume_slicer'))
    
        self.fixed_ijk_to_ras, self.moving_ijk_to_ras = self.getIJKToRASTransform()

    def getMask(self, array, threshold):
        """ inputs:
        ---- array is the array from which to get the mask from
        ---- threshold to make the mask
            return an array of the mask
        """
        mask = (array >= threshold)*1
        #TODO: perhaps use a range of values as threshold instead of a single value
        return mask

    def loadVolume(self, filePath):
        [success, loadedVolumeNode] = slicer.util.loadVolume(filePath, returnNode = True)
        return loadedVolumeNode

    def saveAndLoadMask(self, mask_array, base_volume, path, filename, save):
        """ inputs:
        ---- mask_array is the mask created from the volume
        ---- base_volume is the volume from which the mask is derived from
        ---- path+filename is the location and name of the saved file 
        ---- save is a boolean determining whether to save the mask
            return: a slicer volume of the mask
        """         
        mask_node = self.addVolumeFromArray(base_volume, mask_array, filename)

        if save:
            filename = filename + ".nii"
            slicer.util.saveNode(mask_node, os.path.join(path, filename))

        return mask_node

    def addVolumeFromArray(self, volume, array, name):
        """ inputs:
        ---- volume is a slicer volume where the geometry info is being copied from
        ---- array is a numpy array
        """
        node = slicer.vtkMRMLScalarVolumeNode()
        node.SetOrigin(volume.GetOrigin())
        node.SetSpacing(volume.GetSpacing())
        node.SetName(name)
        slicer.util.updateVolumeFromArray(node, array)
        slicer.mrmlScene.AddNode(node)

        return node

    def grayModelFromMask(self, volumeNode, name, colour):
        parameters = {}
        parameters["InputVolume"] = volumeNode.GetID()
        parameters["Threshold"] = 1
        parameters['Name'] = name
        outModel = slicer.vtkMRMLModelNode()
        outModel.SetName(name)
        parameters["OutputGeometry"] = outModel
        
        slicer.mrmlScene.AddNode( outModel )

        cliNode = (slicer.cli.run(slicer.modules.grayscalemodelmaker, None, parameters,wait_for_completion=True))

        pat = np.array(colour, float)/ 255
        outModel.GetDisplayNode().SetColor(pat)
        outModel.GetDisplayNode().SetOpacity(1)
        return cliNode, outModel

    def printStatus(self, caller, event):
        if caller.IsA('vtkMRMLCommandLineModuleNode'):
            status = caller.GetStatusString()
            print("Status is %s" % status)
        if "with errors" in status:
            print("The error is %s" %caller.GetErrorText())

    def getMean(self, grid, mat):
        mult = np.multiply(grid,mat)
        mean = sum(sum(sum(mult)))/np.count_nonzero(mult)

        return mean

    def checkOrthonormal(self, n4x4matrix, identifier):
        for i in range(3):
            if i < 2:
                result = np.dot(n4x4matrix[:3,i],n4x4matrix[:3,i+1])
            else:
                result = np.dot(n4x4matrix[:3,i], n4x4matrix[:3,0])

            if result > 1e-4:
                print(identifier + 'not ortho @', i)

        for i in range(3):
            result = np.linalg.norm(n4x4matrix[:3,i])

            if int(result) != 1:
                print( identifier + ' not normal @', i)
    
    def getIJKToRASTransform(self):
        ijkMat_moving = vtk.vtkMatrix4x4()
        ijkMat_fixed = vtk.vtkMatrix4x4()
        slicer.util.getNode('movingVolume_slicer').GetIJKToRASMatrix(ijkMat_moving)
        slicer.util.getNode('fixedVolume_slicer').GetIJKToRASMatrix(ijkMat_fixed)
        fixed_ijk_to_ras = slicer.util.arrayFromVTKMatrix(ijkMat_fixed)
        moving_ijk_to_ras = slicer.util.arrayFromVTKMatrix(ijkMat_moving)
        print('fixed_ijk_to_ras', fixed_ijk_to_ras)
        print('moving_ijk_to_ras', moving_ijk_to_ras)
        self.checkOrthonormal(fixed_ijk_to_ras, 'fixed_ijk_to_ras')
        self.checkOrthonormal(moving_ijk_to_ras, 'moving_ijk_to_ras')
        
        return fixed_ijk_to_ras, moving_ijk_to_ras

    def getObjToRAS(self, mask, model, node, identifier):
        """
        Copies the elements of a vtkMatrix4x4 into a numpy array.
    
        :@type matrix: vtk.vtkMatrix4x4
        :@param matrix: The matrix to be copied into an array.
        :@rtype: numpy.ndarray
        """
        cent = vtk.vtkCenterOfMass()
        pd = slicer.util.getNode(model.GetID()).GetPolyData()
        cent.SetInputData(pd)
        cent.Update()
        centroid = np.array(cent.GetCenter()).reshape([3,1])

        # centroid in numpy space
        centroid_np = np.array(ndimage.measurements.center_of_mass(mask))
        centroid_ijk = np.dot( self.numpy_to_ijk, np.append(centroid_np,[1]) )
        
        ijk_to_ras = np.eye(4)
        if identifier == 'moving':
            ijk_to_ras = self.moving_ijk_to_ras
        else:
            ijk_to_ras = self.fixed_ijk_to_ras
        
        # convert centroid to ras space
        centroid_ras = np.array(np.dot(ijk_to_ras, centroid_ijk))[0:3]
        
        # convert volume indices from numpy to ras
        idx_list = np.where(mask > 0)
        idx_append = np.ones(idx_list[0].shape)
        idx_list_np = list(zip(idx_list[0], idx_list[1], idx_list[2], idx_append))
        
        # numpy and ijk have x and z switched
        idx_list_ijk = np.dot( self.numpy_to_ijk, np.array(idx_list_np).T )
        # convert to ras
        idx_list_ras = np.dot( ijk_to_ras, idx_list_ijk).T
        
        x_p = idx_list_ras[:,0] - centroid_ras[0]
        y_p = idx_list_ras[:,1] - centroid_ras[1]
        z_p = idx_list_ras[:,2] - centroid_ras[2]

        x_p_squared = np.zeros(x_p.shape)
        y_p_squared = np.zeros(y_p.shape)
        z_p_squared = np.zeros(z_p.shape)

        np.square(x_p, x_p_squared)
        np.square(y_p, y_p_squared)
        np.square(z_p, z_p_squared)
        
        I = np.zeros([3,3],dtype=float)

        I[0,0] = np.mean(x_p_squared)
        I[0,1] = np.mean(x_p*y_p)
        I[0,2] = np.mean(x_p*z_p)
        I[1,2] = np.mean(y_p*z_p)
        I[1,0] = I[0,1]
        I[2,0] = I[0,2]
        I[2,1] = I[1,2]
        I[1,1] = np.mean(y_p_squared)
        I[2,2] = np.mean(z_p_squared)

        eigvals, eigvecs = np.linalg.eigh(I)
        index = np.abs(eigvals).argsort()[::-1]
        eigvecs = eigvecs[:,index]

        if np.linalg.det(eigvecs) < 0:
            eigvecs[:,-1] = -eigvecs[:,-1]
        
        obj_to_ras = np.eye(4)
        obj_to_ras[0:3,3] = centroid_ras
        obj_to_ras[0:3,0:3] = eigvecs
        
        return obj_to_ras, idx_list_ras[:,0:3]

    def addSlicerTransform(self, npMatrix4x4, name):
        vtkMat = vtk.vtkMatrix4x4()
        vtkMat.DeepCopy(npMatrix4x4.flatten())

        slicerTransform = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLTransformNode', name)
        slicerTransform.ApplyTransformMatrix(vtkMat)

        return slicerTransform

    def setRulerProperty(self, ruler, name, i, palette):
        ruler.SetLocked(1)
        ruler.GetAnnotationTextDisplayNode().SetOpacity(0)
        ruler.SetName(name+'_'+ str(i))

        dpNode = ruler.GetDisplayNode()
        dpNode.SetColor(palette)
        dpNode.SetLineThickness(3)
        dpNode.SetLineWidth(3.0)
        dpNode.SetLabelVisibility(0)
        dpNode.SetMaxTicks(0)

    def getMovingToFixedTransform(self, moving_obj_to_ras, fixed_obj_to_ras):
        # compute moving_ras_to_fixed_ras assuming fixed_obj == moving_obj
        moving_ras_to_obj = np.linalg.inv(moving_obj_to_ras)
        moving_ras_to_fixed_ras = np.dot(fixed_obj_to_ras, moving_ras_to_obj)
        self.checkOrthonormal(moving_ras_to_fixed_ras, 'moving_ras_to_fixed_ras')

        moving_ras_to_fixed_ras_vmatrix = vtk.vtkMatrix4x4()
        moving_ras_to_fixed_ras_transform_node = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLTransformNode', 'MovingToFixed')
        slicer.util.updateVTKMatrixFromArray(moving_ras_to_fixed_ras_vmatrix, moving_ras_to_fixed_ras)
        moving_ras_to_fixed_ras_transform_node.SetMatrixTransformToParent(moving_ras_to_fixed_ras_vmatrix)

        return moving_ras_to_fixed_ras_transform_node

    def visualizeVectors(self, obj_to_ras, name, colour, transformToFixed=None):
        palette = np.array(colour, float)/ 255

        for i in range(3):

            ruler = slicer.vtkMRMLAnnotationRulerNode()
            ruler.SetPosition1( obj_to_ras[0:3,3].tolist() )
            seg = self.scalingFactor*obj_to_ras[0:3,i]+obj_to_ras[0:3,3]
            ruler.SetPosition2( seg.tolist() )
            ruler.Initialize(slicer.mrmlScene)
            
            if transformToFixed is not None:
                ruler.SetAndObserveTransformNodeID(transformToFixed.GetID())

            self.setRulerProperty(ruler, name, i, palette)
        
    
    def calculateDotProduct(self, axis, points):
        result = np.dot( points.reshape([-1,3]), axis.reshape([3,1]) )
        return result
        
    def correctPAxisDirection(self, moving_obj_to_ras, fixed_obj_to_ras, moving_idx_list, fixed_idx_list):
        limit_moving = int(moving_idx_list.shape[0] * 0.2)
        limit_fixed = int(fixed_idx_list.shape[0] * 0.2)
        
        limit = limit_moving
        if limit_moving >= limit_fixed:
            limit = limit_fixed
        moving_idx_list = moving_idx_list[0:limit,:]
        fixed_idx_list = fixed_idx_list[0:limit,:]
        
        for i in range(3):
            fixed_axis_var = self.calculateDotProduct(fixed_obj_to_ras[0:3,i], fixed_idx_list).flatten()
            
            moving_axis_var_1 = self.calculateDotProduct(moving_obj_to_ras[0:3,i], moving_idx_list).flatten()
            moving_axis_var_2 = self.calculateDotProduct(-moving_obj_to_ras[0:3,i], moving_idx_list).flatten()
            
            correlation_1 = np.correlate(fixed_axis_var, moving_axis_var_1)
            correlation_2 = np.correlate(fixed_axis_var, moving_axis_var_2)
            
            if  correlation_2 > correlation_1:
                moving_obj_to_ras[0:3,i] = -moving_obj_to_ras[0:3,i]
                print('direction changed @', i)
        
        return moving_obj_to_ras, fixed_obj_to_ras
        
    def run(self, fixedVolmPath, movingVolmPath, imageThreshold, vectorScalingFactor, saveDataFlag):
        self.loadObjects(fixedVolmPath, movingVolmPath, imageThreshold, vectorScalingFactor)

        start = time.time()
        movingVolm_mask_np = self.getMask(self.movingVolume, self.threshold)
        fixedVolm_mask_np = self.getMask(self.fixedVolume, self.threshold)
        end = time.time()
        print('mask creation time: ', (end-start))

        start = time.time()
        movingVolm_mask = self.saveAndLoadMask(movingVolm_mask_np, self.movingVolume_slicer, movingVolmPath, 'movingVolm_mask', saveDataFlag)
        fixedVolm_mask = self.saveAndLoadMask(fixedVolm_mask_np, self.fixedVolume_slicer, fixedVolmPath, 'fixedVolm_mask', saveDataFlag)
        end = time.time()
        print('save and load mask time: ', (end-start))
        
        # create grayscale models
        start = time.time()
        cliStatus, movingModel = self.grayModelFromMask(movingVolm_mask, 'MovingVolmModel', (241, 214, 145))
        cliStatus, fixedModel = self.grayModelFromMask(fixedVolm_mask, 'FixedVolmModel', (128, 174, 128))
        end = time.time()
        print('model creation time: ', (end-start))

        # compute the Inertial matrix of the mask
        start = time.time()
        moving_obj_to_ras, moving_idx_list = self.getObjToRAS(movingVolm_mask_np, movingModel, self.movingVolume_slicer, 'moving')
        fixed_obj_to_ras, fixed_idx_list = self.getObjToRAS(fixedVolm_mask_np, fixedModel, self.fixedVolume_slicer, 'fixed')
        end = time.time()
        print('intertial matrix calculation time: ', (end-start))

        # correct for direction of the eig vectors
        
        start = time.time()
        moving_obj_to_ras, fixed_obj_to_ras = self.correctPAxisDirection(moving_obj_to_ras, fixed_obj_to_ras, moving_idx_list, fixed_idx_list)
        end = time.time()
        print('calculating correlation time:', (end-start))
        
        # get the transform to go from moving to fixed volume
        start = time.time()
        self.transformFromMtoF = self.getMovingToFixedTransform(moving_obj_to_ras, fixed_obj_to_ras)
        end = time.time()
        print('find and apply transform time: ', (end-start))
        
        # apply transform to the slicer grayscale model
        movingModel.SetAndObserveTransformNodeID(self.transformFromMtoF.GetID())
        movingVolm_mask.SetAndObserveTransformNodeID(self.transformFromMtoF.GetID())
        self.movingVolume_slicer.SetAndObserveTransformNodeID(self.transformFromMtoF.GetID())

        self.visualizeVectors(moving_obj_to_ras, 'moving_vects', (241, 214, 145), transformToFixed=self.transformFromMtoF)
        self.visualizeVectors(fixed_obj_to_ras, 'fixed_vects', (128, 174, 128), transformToFixed=None)

        print('completed')