## How to setup
+ Download 3D Slicer (at least **4.11.0** nightly build version from the official site [https://download.slicer.org/]
+ In the Python Interactor in Slicer, type 
    ```
        pip_install("scipy")
    ```
    and 
    ``` 
        pip_install("nibabel")
    ```
+ Add the custom module: 
    + Open slicer
    + Click Edit from the top menu bar
    + Select application setting
    + In the settings window, go to Modules
    + In "Additional Module Path", click add to add the path to the **module** folder (e.g. where/this/module/is/cloned/to/paxistransform/PAxisModule)
    + In the settings window, go to Developer
    + Select the enable developer mode check box
    + Restart slicer when asked 
+ In the module list, search for PAxis Transform

## How to run
+ Fixed volume is the target orientation you are aligning to. Input a path to the .nii (Nifti) file you are working with.
+ Moving volume is the volume whose orientation needs to be corrected to the fixed volume. Input the other .nii file.
+ Threshold is the value at which the volume of interest will be generated in. You can use the segmentation module to assist your decision for selecting the appropriate threshold. 
    + For instance, the default value of 900 will roughly select all the bones in a CT scan.
+ Vector scaling factor is the value that the unit vectors of the principal directions are scaled by for visualization purposes.

## What to expect
+ The module will create grayscale models of the input volumes (fixed volume associated with the green color and moving associated with yellow) and vectors pointing in the principal directions.
+ Under the transform module, you will see the corresponding transform that has been applied to the volume 
+ Under the transform module, you will also see two transform associated with the eigen vectors of the volume

## Transform diagram
![Transform diagram](tfdiagram_paxis.jpeg)